import { Meteor } from 'meteor/meteor';
Resolutions = new Mongo.Collection('resolutions')
Meteor.startup(() => {
  // code to run on server at startup
});

Meteor.methods({
    newResolution: function(resTitle){
        var currentUserId = Meteor.userId();
        Resolutions.insert({
            title: resTitle,
            createdAt : new Date()
        });
    },

    deleteResolution : function(id){
    	Resolutions.remove(id);
    },
    doneResolution: function(id, checked){
    	Resolutions.update(id, {$set : {checked : checked}});

    }
});
