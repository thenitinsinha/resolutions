import { Mongo } from 'meteor/mongo';

Resolutions = new Mongo.Collection('resolutions')  //note - add this line in server also.

if (Meteor.isClient) {
	Template.body.helpers({
		resolutions : function(){
			if(Session.get('sessionHideFinished')){
				return Resolutions.find({checked : {$ne : true}});
			}
			else{
				return Resolutions.find({});   // we are basically taking from the database using the collection named "Resolutions"	
			}
			
		},
		hideFinished : function(){
			return Session.get('sessionHideFinished');
		}
	});

	Template.body.events({
		'submit .new-resolution' : function(event){    //.new-resolution is the class of form defined in html file
			var title = event.target.title.value;

			/* We have removed this following code from client and put this functionality in a different function
			/* We will call that function now and get our thing done.
			Resolutions.insert({
				title : title,
				createdAt : new Date(),
			});
			*/
			Meteor.call('newResolution', title) // 1st argument is the function name and 2nd argument is actually the parameter/argument for that function
			event.target.title.value = "";
			return false;
		},

		'change .hide-finished' : function(event){
			console.log("hide-finished has been changed")
			Session.set('sessionHideFinished', event.target.checked); // first argument can be any name we want. only valid for this session
		}
	});

	Template.resolution.events({      //here we are playing with the template called "resolution" not the body
		'click .delete' : function(){
			//Resolutions.remove(this._id); //in this line "this" represents the resolution linked to the resolution we see ....
			//COMMENTED ABOVE LINE TO MAKE THIS ACTION SECURED AND PUT THE LOGIC INSIDE METEOR METHOD
			Meteor.call('deleteResolution',this._id)
		},

		'click .toggle-checked' : function(){
			Meteor.call('doneResolution',this._id, !this.checked)
		}
	})

	Accounts.ui.config({
		passwordSignupFields : "USERNAME_ONLY"
	});
}
/*
if (Meteor.isServer){
	Meteor.startup(function(){
		//code to run on the server...
	});
}
*/

Meteor.methods({
    newResolution: function(resTitle){
        var currentUserId = Meteor.userId();
        Resolutions.insert({
            title: resTitle,
            createdAt : new Date()
        });
    },

    deleteResolution : function(id){
    	Resolutions.remove(id);
    },
    doneResolution: function(id, checked){
    	Resolutions.update(id, {$set : {checked : checked}});

    }
});



/*
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';
*/


/*
Lecture 4 -- about creating an array here and show on the page

if (Meteor.isClient) {
	Template.body.helpers({
		resolutions : [
		{title : "hello resolution no 1 "},  //this is an array with a single object. lets output it in html
		{title : "hello resolution no 2 "}, 
		{title : "just fucking around "},	 
		]
	});
}

*/

/*
this is lecture 3....

Template.hello.onCreated(function helloOnCreated() {
  // counter starts at 0
  this.counter = new ReactiveVar(0);
});

Template.hello.helpers({
  counter() {
    return Template.instance().counter.get();
  },
});

Template.hello.events({
  'click button'(event, instance) {
    // increment the counter when button is clicked
    instance.counter.set(instance.counter.get() + 1);
  },
});
*/